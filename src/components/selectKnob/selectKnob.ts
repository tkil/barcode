import { html, render } from 'lit-html'

const componentName = 'select-knob'

const style = /*css*/`
  button {
    background-color: #3498DB;
    border-radius: 48px;
    border: 2px solid #2980B9;
    color: white;
    cursor: pointer;
    display: block;
    font-size: 1.8rem;
    min-width: 240px;
    margin: 24px auto;
    padding: 8px 24px;
  }
`

const template = (label: string) => html`
  <style>${style}</style>
  <button>${label}</button>
`

// class MyComponent extends HTMLElement { ... }
// window.customElements.define('my-component', MyComponent);
window.customElements.define(componentName, class extends HTMLElement {
  private _shadow: ShadowRoot
  private _label: string

  constructor() {
    super()
    this._shadow = this.attachShadow({mode: 'open'})
    // Attribute Mapping
    this._label = this.getAttribute('label') || '< missing >'
    // An instance of the element is created or upgraded.
    // Useful for initializing state, setting up event listeners, or creating a shadow dom.
    // See the spec for restrictions on what you can do in the constructor.
  }

  public connectedCallback() {
    render(template(this._label), this._shadow)
    const btn = this._shadow.querySelector('button')
    if (btn) {
      btn.addEventListener('click', (event) => {
        event.stopPropagation()
        this._click()
      })
    }
  }

  public disconnectedCallback() {
    // Called every time the element is removed from the DOM. Useful for running clean up code.
  }

  public observedAttributes() {
    return ['label']
    // Elements can react to attribute changes by defining a attributeChangedCallback.
    // The browser will call this method for every change to attributes listed in the observedAttributes array.
  }

  public attributeChangedCallback(attrName: string, oldVal: string, newVal: string)	{
    switch (attrName) {
      case 'label':
        this._label = newVal
        return
    }
  }

  private _click() {
    this.dispatchEvent(new CustomEvent('click', {
      detail: { value: 'my-val'}
    }))
  }
})
