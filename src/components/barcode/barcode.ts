import jsBarcode from 'jsbarcode'
import { html, render } from 'lit-html'

const componentName = 'barcode-component'

const style = /*css*/`
  svg {
    display: block;
    margin: 0 auto;
  }
`

const template = () => html`
  <style>${style}</style>
  <svg id="barcode"></svg>
`

window.customElements.define(componentName, class extends HTMLElement {
  private _shadow: ShadowRoot
  private _code: string = ''
  private _format: string = ''
  private _label: string = ''
  private _width: number = 0
  private _height: number = 0

  constructor() {
    super()
    this._shadow = this.attachShadow({mode: 'open'})
  }

  public connectedCallback() {
    // Attribute Mapping
    this._code = this.getAttribute('code') || ''
    this._format = this.getAttribute('format') || ''
    this._label = this.getAttribute('label') || ''
    this._height = Number.parseFloat(this.getAttribute('height') || '1')
    this._width = Number.parseFloat(this.getAttribute('width') || '1')

    render(template(), this._shadow)
    const svgEl = this._shadow.querySelector('svg')
    jsBarcode(svgEl, this._code, {
      font: 'arial',
      format: this._format,
      height: this._height,
      text: this._label,
      textMargin: 12,
      width: this._width
    })
  }

  public observedAttributes() {
    return ['code', 'format', 'height', 'width']
  }

  public attributeChangedCallback(attrName: string, oldVal: string, newVal: string)	{
    switch (attrName) {
      case 'code':
        this._code = newVal
        return
      case 'format':
        this._format = newVal
        return
      case 'label':
        this._label = newVal
        return
      case 'height':
        this._height = Number.parseFloat(this.getAttribute('height') || '1')
        return
      case 'width':
        this._width = Number.parseFloat(this.getAttribute('width') || '1')
        return
    }
  }
})
