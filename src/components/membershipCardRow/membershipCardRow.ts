import { html, render } from 'lit-html'

const componentName = 'membership-card-row'

const style = /*css*/`
  div {
    background-color: white;
    border-top: 1px solid lightgrey;
    color: #444444;
    display: flex;
    font-family: arial;
    font-weight: 300;
    justify-content: space-between;
    padding: 10px;
  }
`

const template = (label: string, value: string) => html`
  <style>${style}</style>
  <div>
    <span class="label">${label}</span>
    <span class="value">${value}</span>
  </div>
`

window.customElements.define(componentName, class extends HTMLElement {
  private _shadow: ShadowRoot
  private _label: string
  private _value: string

  constructor() {
    super()
    this._shadow = this.attachShadow({mode: 'open'})
    // Attribute Mapping
    this._label = this.getAttribute('label') || ''
    this._value = this.getAttribute('value') || ''
  }

  public connectedCallback() {
    render(template(this._label, this._value), this._shadow)
  }

  public observedAttributes() {
    return ['label', 'value']
  }

  public attributeChangedCallback(attrName: string, oldVal: string, newVal: string)	{
    switch (attrName) {
      case 'label':
        this._label = newVal
        return
      case 'value':
        this._value = newVal
        return
    }
  }
})
