import { html, render } from 'lit-html'

const componentName = 'membership-card'

const style = /*css*/`
  header {
    background-color: #4B4B4B;
    color: white;
    font-family: arial;
    padding: 12px;
    text-align: center;
  }

  barcode-component {
    display: block;
    padding: 80px 0 40px;
    background-color: white;
  }

  .bottom-border {
    border-top: 1px solid lightgrey;
  }

  footer {
    background-color: #4B4B4B;
    bottom: 0;
    height: 40px;
    position: absolute;
    min-width: 400px;
  }
`

const template = (code: string, codeFormat: string, label: string) => html`
  <style>${style}</style>
  <header>Membership Card</header>
  <barcode-component
    label="${label}"
    code="${code}"
    format="${codeFormat}"
    height="60"
    width="1.4"
  ></barcode-component>
  <membership-card-row
    label="Member Number"
    value="${code}"
  ></membership-card-row>
  <membership-card-row
    label="Membership Level"
    value="Gympass"
  ></membership-card-row>
  <div class="bottom-border"><div>
  <footer></footer>
`

window.customElements.define(componentName, class extends HTMLElement {
  private _shadow: ShadowRoot
  private _label: string
  private _code: string
  private _codeFormat: string

  constructor() {
    super()
    this._shadow = this.attachShadow({mode: 'open'})
    // Attribute Mapping
    this._label = this.getAttribute('label') || ''
    this._code = this.getAttribute('code') || ''
    this._codeFormat = this.getAttribute('code-format') || ''
  }

  public connectedCallback() {
    render(template(this._code, this._codeFormat, this._label), this._shadow)
  }

  public observedAttributes() {
    return ['label', 'code', 'code-format']
  }

  public labelibuteChangedCallback(labelName: string, oldVal: string, newVal: string)	{
    switch (labelName) {
      case 'label':
        this._label = newVal
        return
      case 'code':
        this._code = newVal
        return
      case 'code-format':
        this._codeFormat = newVal
        return
    }
  }
})
