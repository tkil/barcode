import '@components/register'
import '@styles/reset.css'
import '@styles/main.css'

const main = () => {
  // console.log('HELLO')
  // jsBarcode("#barcode", "109079043", { format:'CODE39', width:1.6, height: 120 });
  const el = document.querySelector('select-knob')
  if (el) {
    el.addEventListener('click', (event: any) => {
      console.log(event.detail.value)
    })
  }
}

window.addEventListener('DOMContentLoaded', main)
